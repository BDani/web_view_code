//
//  AppDelegate.h
//  Web_View_Code
//
//  Created by Bruno Tavares on 22/01/15.
//  Copyright (c) 2015 Bruno Tavares. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

