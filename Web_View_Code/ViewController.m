//
//  ViewController.m
//  Web_View_Code
//
//  Created by Bruno Tavares on 22/01/15.
//  Copyright (c) 2015 Bruno Tavares. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@property (nonatomic, strong) UIWebView *webView;
@property (nonatomic, strong) UILabel *loadingLabel;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor whiteColor];
    self.view.translatesAutoresizingMaskIntoConstraints = NO;
    
    // configure web view
    self.webView = [[UIWebView alloc] init];
    self.webView.translatesAutoresizingMaskIntoConstraints = NO;
    self.webView.delegate = self;
    
    NSString *urlString = @"https://www.betfair.com/exchange/";
    
    NSURL *url = [NSURL URLWithString:urlString];
    
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    
    [self.webView loadRequest:request];
    
    
    // configure loading label
//    self.loadingLabel = [[UILabel alloc] init];
//    self.loadingLabel.translatesAutoresizingMaskIntoConstraints = NO;
//    
//    self.loadingLabel.hidden = true;
//    self.loadingLabel.textColor = [UIColor whiteColor];
//    self.loadingLabel.text = @"Loading...";
    
    // add objects to the view
    [self.view addSubview:self.webView];
    //[self.view addSubview:self.loadingLabel];
    
    
    // add constraints to web view
    NSDictionary *viewsDictionary = @{@"webView":self.webView,};
    
    NSArray *constraint_V_webView = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|-20-[webView]|"
                                                                    options:0
                                                                    metrics:nil
                                                                      views:viewsDictionary];
    
    NSArray *constraint_H_webView = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|[webView]|"
                                                                    options:0
                                                                    metrics:nil
                                                                      views:viewsDictionary];
    
    [self.view addConstraints:constraint_H_webView];
    [self.view addConstraints:constraint_V_webView];
    //[self.view addConstraints:constraint_H_label];
    //[self.view addConstraints:constraint_V_label];
}

- (void)webViewDidStartLoad:(UIWebView *)webView {
    
    
}

- (void)webViewDidFinishLoad:(UIWebView *)webView {
    
    
}

@end
